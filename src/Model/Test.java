package Model;

import java.io.Serializable;

/**
 * @author Karol Ornoch
 * Klasa opisujaca Test jego id oraz nazwę testu.
 */
public class Test implements Serializable {
    private int id;
    private String name;

    public Test(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return id + " " + name;
    }
}
