package Model;

import java.io.Serializable;

/**
 *
 * @author Karol Ornoch
 * Klasa Opisujaca użytkownika systemu jeso login , hasło oraz czy czy jest adminem.
 *
 */
public class

User implements Serializable {
    private int id;
    private String login;
    private String password;
    private boolean isAdmin;

    User() {
    }

    public User(int id, String login, String password, boolean isAdmin) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.isAdmin = isAdmin;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Override
    public String toString() {
        return "id: " + getId() +
                "\nlogin: " + getLogin() +
                "\npassword: " + getPassword() +
                "\nisAdmin: " + isAdmin() +
                "\n";
    }
}
