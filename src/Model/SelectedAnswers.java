package Model;

import java.io.Serializable;

/**
 * @author Karol Ornoch
 * Klasa opisujaca wybrana odpowiedz posiadajaca id pytania , id testu oraz poprawna odpowiedz
 */
public class SelectedAnswers implements Serializable {
    private int id;
    private int idtest;
    private String answer;

    public SelectedAnswers(int id, int idtest, String answer) {
        this.id = id;
        this.answer = answer;
        this.idtest = idtest;
    }

    SelectedAnswers() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getIdtest() {
        return idtest;
    }

    public void setIdtest(int idtest) {
        this.idtest = idtest;
    }
}
