package Model;

import java.io.Serializable;

/**
 * @author Karol Ornoch
 */
public class Mark implements Serializable {
    private int id;
    private int idtest;
    private int iduser;
    private int mark;

    public Mark(int id, int idtest, int iduser, int mark) {
        this.id = id;
        this.idtest = idtest;
        this.iduser = iduser;
        this.mark = mark;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdtest() {
        return idtest;
    }

    public void setIdtest(int idtest) {
        this.idtest = idtest;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }
}
