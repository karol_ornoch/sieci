package Model;

import java.io.Serializable;

/**
 * @author Karol Ornoch
 * Klasa opisujaca wiadomość która bedzie wykorzystywana do przesyawia wiadomości miedzy klientem a serverem zawieracaja typ wiadomości oraz wiadomość.
 */
public class Message implements Serializable {
    private int type;
    private Object message;

    public Message(int type, Object message) {
        this.type = type;
        this.message = message;
    }

    public Message() {
    }

    public int getType() {
        return type;
    }

    public Object getMessage() {
        return message;
    }
}
