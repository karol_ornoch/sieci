package Client;

import Model.Question;
import Model.Test;
import Model.User;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

/**
 *
 * Interfejs z metodami do zaimplementowania
 * @author Karol Ornoch
 *
 */
public interface Listener {
    /**
     * Metoda do logowania uzytkownika systemu.
     */
    void reactOnLogin(User msg) throws IOException;
    /**
     * Metoda reagujaca na przyjscie wiadomości i informacja od serwera.
     */
    void reactOnComment(String msg);

    void reactOnRegister(String msg);
    /**
     * Metoda reagujaca na przyjscie listy pytan z serwera.
     */
    void reactOnQuestionsList(List<Question> questions);
    /**
     * Metoda reagujaca na przyjscie listy testow z serwera.
     */
    void reactOnTestList(Vector<Test> tests) throws IOException;
    /**
     * Metoda reagujaca na przyjscie zaktualizowanej listy pytan z serwera.
     */
    void reacttOnUpdate(Vector<Test> tests) throws IOException;



}
