package Client;

import Model.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * @author Karol Ornoch
 */

public class Client implements ActionListener, Listener {
    private static JFrame frame;
    private JButton btnLogin;
    private JButton btnRegister;
    private JTextField fieldLogin;
    private JTextField fieldPassword;
    private JLabel lblLogin;
    private JLabel lblPassword;
    private static Sender dataSender;
    private static User user;

    public static void main(String[] args) {
        new Client();
    }

    Client() {

        frame = new JFrame("Logowanie do aplikacji Testy");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(300, 200);
        lblLogin = new JLabel("Login:");
        lblPassword = new JLabel("Hasło:");
        btnLogin = new JButton("Zaloguj");
        btnRegister = new JButton("Zarejestruj");
        fieldLogin = new JTextField(20);
        fieldPassword = new JTextField(20);

        JPanel panel = new JPanel();
        lblLogin.setForeground(Color.WHITE);
        lblPassword.setForeground(Color.WHITE);
        fieldLogin.setForeground(Color.BLACK);
        fieldPassword.setForeground(Color.BLACK);
        panel.add(lblLogin);
        panel.add(fieldLogin);
        panel.add(lblPassword);
        panel.add(fieldPassword);
        panel.add(btnLogin);
        panel.add(btnRegister);
        panel.setBackground(Color.DARK_GRAY);
        btnLogin.addActionListener(this);
        btnRegister.addActionListener(this);
        frame.add(panel);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);
        frame.setVisible(true);

        try {
            dataSender = new ClientThread(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnLogin) {
            if (fieldLogin.getText().trim().equals("")) {
                JOptionPane.showMessageDialog(null, "Wypełnij wszystkie pola", "Błąd", JOptionPane.ERROR_MESSAGE);
            } else {
                dataSender.send(new Message(0, new User(0, fieldLogin.getText(), fieldPassword.getText(), false)));
            }
        }
        if (e.getSource() == btnRegister) {
            showRegisterPanel();
        }
    }


    private void showRegisterPanel() {
        new RegisterPanel();
    }

    @Override
    public void reactOnLogin(User u) throws IOException {
        if (u != null) {
            user = u;
            frame.setVisible(false);
            dataSender.send(new Message(4, "OK"));

        } else {
            JOptionPane.showMessageDialog(null, "Błędne dane logowania !!", "Błąd", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void reactOnRegister(String msg) {
        JOptionPane.showMessageDialog(null, msg, "Sukces", JOptionPane.INFORMATION_MESSAGE);
    }

    @Override
    public void reactOnQuestionsList(List<Question> questions) {
        new TestGUI(Integer.toString(questions.get(0).getIdTest()), questions);
    }

    @Override
    public void reactOnTestList(Vector<Test> tests) throws IOException {
        new AdminPanel(tests);
    }

    @Override
    public void reacttOnUpdate(Vector<Test> tests) throws IOException {
        AdminPanel.getInstance();
        AdminPanel.setTests(tests);
    }

    @Override
    public void reactOnComment(String msg) {
        JOptionPane.showMessageDialog(null, msg, "Wiadomość od serwera", JOptionPane.INFORMATION_MESSAGE);
    }

    private class RegisterPanel extends JFrame implements ActionListener {
        private JPanel panel;
        private JLabel lblLogin;
        private JLabel lblPassword;
        private JTextField fieldLogin;
        private JTextField fieldPassword;
        private JButton btnRegister;

        RegisterPanel() {

            panel = new JPanel();
            lblLogin = new JLabel("Login: ");
            lblPassword = new JLabel("Hasło: ");
            fieldLogin = new JTextField(10);
            fieldPassword = new JTextField(10);
            btnRegister = new JButton("Zarejestruj");

            panel.add(lblLogin);
            panel.add(fieldLogin);
            panel.add(lblPassword);
            panel.add(fieldPassword);

            panel.add(btnRegister);
            this.add(panel);
            btnRegister.addActionListener(this);
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            setSize(400, 200);
            Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
            int x = (int) ((dimension.getWidth() - getWidth()) / 2);
            int y = (int) ((dimension.getHeight() - getHeight()) / 2);
            setLocation(x, y);
            setTitle("Zarejestruj się");
            setVisible(true);

        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == btnRegister) {

                if (fieldPassword.getText().length() < 6 || fieldLogin.getText().length() < 6) {
                    JOptionPane.showMessageDialog(null, "Login badź hasło są zbyt krótkie!!", "Błąd", JOptionPane.ERROR_MESSAGE);
                } else {
                    dataSender.send(new Message(1, new User(0, fieldLogin.getText(), fieldPassword.getText(), false)));
                    this.dispose();
                }
            }
        }
    }
    private static class AdminPanel extends JFrame implements ActionListener {
        private JPanel panel;
        private JMenuBar menuBar;
        private JMenu file;
        private JMenuItem logout;
        private JMenuItem exit;
        private JPanel panelGroup;
        private static JList<Test> list;
        private static JScrollPane listScroller;
        private JButton addTest;
        private JButton addQuestion;
        private JButton beginTest;
        static Vector<Test> vector;


        private static AdminPanel instance;

        private static AdminPanel getInstance() throws IOException {
            if(instance==null)
            instance = new AdminPanel();
            return instance;
        }

        AdminPanel(){}

        static void setTests(Vector<Test> tests){
            vector=tests;
            list.removeAll();
            list.setListData(tests);
        }

        AdminPanel(Vector<Test> tests) throws IOException {
            frame.setVisible(false);
            panel = new JPanel(new BorderLayout());
            add(panel);
            vector = tests;
            list = new JList<>(tests);
            listScroller = new JScrollPane(list);
            if (user.isAdmin()) {
                addTest = new JButton("Dodaj Test");
                addQuestion = new JButton("Dodaj pytanie ");
                setTitle("Panel Administratora");
                addTest.addActionListener(this);
                addQuestion.addActionListener(this);
                panelGroup = new JPanel();
                panelGroup.add(addTest);
                panelGroup.add(addQuestion);
                panel.add(panelGroup, BorderLayout.SOUTH);
            } else {
                beginTest = new JButton("Rozwiąż Test");
                setTitle("Panel Użytkownika");
                beginTest.addActionListener(this);
                panel.add(BorderLayout.SOUTH, beginTest);
            }
            panel.add(BorderLayout.CENTER, listScroller);
            menuBar = new JMenuBar();
            file = new JMenu("Plik");
            logout = new JMenuItem("Wyloguj");
            exit = new JMenuItem("Zakończ");
            file.add(logout);
            file.add(exit);
            menuBar.add(file);
            logout.addActionListener(this);
            exit.addActionListener(this);
            panel.add(BorderLayout.NORTH,menuBar);
            setSize(400, 400);
            Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
            int x = (int) ((dimension.getWidth() - getWidth()) / 2);
            int y = (int) ((dimension.getHeight() - getHeight()) / 2);
            setLocation(x, y);
            setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

            setVisible(true);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == addTest) {
                try {
                    String name = JOptionPane.showInputDialog(null, "Podaj nazwe testu", JOptionPane.YES_OPTION);

                    if (name.length()<5) {
                        JOptionPane.showMessageDialog(null, "nazwa testu nie moze byc krotsza niz 5 znaków");
                    } else {
                        dataSender.send(new Message(6, name));
                    }

                }catch (NullPointerException e1){
                    JOptionPane.showMessageDialog(null, "nazwa testu nie moze byc pusta i krotsza niz 5 znakow");
                }

            }
            if (e.getSource() == addQuestion) {
                try {
                    int idTest = vector.get(list.getSelectedIndex()).getId();
                    new AddQuestion(idTest);
                } catch (ArrayIndexOutOfBoundsException ex) {
                    JOptionPane.showMessageDialog(null, "Przed dodaniem pytania wybierz Test");
                }

            }
            if (e.getSource() == beginTest) {
                dataSender.send(new Message(3, vector.get(list.getSelectedIndex()).getId()));
            }
            if(e.getSource() == logout){
                user=null;
                frame.setVisible(true);
                this.setVisible(false);
                dataSender.send(new Message(8,"logout"));
            }
            if(e.getSource()== exit){
                dataSender.send(new Message(9,"exit"));
                dataSender.close();
                dataSender=null;
                System.exit(0);
            }

        }
    }

    private static class AddQuestion extends JFrame implements ActionListener {
        private int idTest;
        private JPanel panel;
        private JPanel panel2;
        private JTextField question;
        private JTextField ansver1;
        private JTextField ansver2;
        private JTextField ansver3;
        private JTextField ansver4;
        private JRadioButton a;
        private JRadioButton b;
        private JRadioButton c;
        private JRadioButton d;
        private JButton saveQustion;
        private ButtonGroup buttonGroup;

        AddQuestion(int idTest) {
            panel = new JPanel(new BorderLayout());
            panel2 = new JPanel();
            question = new JTextField("Treść pytania");
            ansver1 = new JTextField("Odpowiedz a",30);
            ansver2 = new JTextField("Odpowiedz b",30);
            ansver3 = new JTextField("Odpowiedz c",30);
            ansver4 = new JTextField("Odpowiedz d",30);
            a = new JRadioButton("a");
            b = new JRadioButton("b");
            c = new JRadioButton("c");
            d = new JRadioButton("d");
            a.setSelected(true);
            b.setSelected(false);
            c.setSelected(false);
            d.setSelected(false);
            saveQustion = new JButton("Zapisz");
            saveQustion.addActionListener(this);
            panel2.add(a);
            panel2.add(ansver1);
            panel2.add(b);
            panel2.add(ansver2);
            panel2.add(c);
            panel2.add(ansver3);
            panel2.add(d);
            panel2.add(ansver4);
            buttonGroup = new ButtonGroup();
            buttonGroup.add(a);
            buttonGroup.add(b);
            buttonGroup.add(c);
            buttonGroup.add(d);
            panel.add(BorderLayout.NORTH, question);
            panel.add(BorderLayout.CENTER, panel2);
            panel.add(BorderLayout.SOUTH, saveQustion);
            add(panel);
            this.idTest = idTest;
            setTitle("Dodawanie pytania");
            setSize(400, 300);
            setResizable(false);
            setVisible(true);
        }


        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == saveQustion) {
                try {
                    if(question.getText().trim().length()<1 || ansver1.getText().trim().length()<1 || ansver2.getText().trim().length()<1 || ansver3.getText().trim().length()<1 || ansver4.getText().trim().length()<1){
                        JOptionPane.showMessageDialog(null, "Wypelnij wszystkie pola przed wyslaniem pytania");
                    }else {
                        if (a.isSelected()) {
                            dataSender.send(new Message(7, new Question(0, question.getText(), ansver1.getText(), ansver2.getText(), ansver3.getText(), ansver4.getText(), ansver1.getText(), idTest)));
                        } else if (b.isSelected()) {
                            dataSender.send(new Message(7, new Question(0, question.getText(), ansver1.getText(), ansver2.getText(), ansver3.getText(), ansver4.getText(), ansver2.getText(), idTest)));
                        } else if (c.isSelected()) {
                            dataSender.send(new Message(7, new Question(0, question.getText(), ansver1.getText(), ansver2.getText(), ansver3.getText(), ansver4.getText(), ansver3.getText(), idTest)));
                        } else if (d.isSelected()) {
                            dataSender.send(new Message(7, new Question(0, question.getText(), ansver1.getText(), ansver2.getText(), ansver3.getText(), ansver4.getText(), ansver4.getText(), idTest)));
                        }
                        this.setVisible(false);
                        dispose();
                    }

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
            }

        }
    }

    private class TestGUI extends JFrame implements ActionListener {
        private JPanel panel;
        private ButtonGroup buttonGroup;
        private JPanel panelGroup;
        private JPanel panelGroup2;
        private JLabel question;
        private JRadioButton a;
        private JRadioButton b;
        private JRadioButton c;
        private JRadioButton d;
        private JButton btnNext;
        List<Question> questions;
        List<SelectedAnswers> selectedAnswersList = new ArrayList<>();
        int id;

        TestGUI(String name, List<Question> questionList) {
            questions = questionList;
            setTitle(name);
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            setSize(800, 600);
            Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
            int x = (int) ((dimension.getWidth() - getWidth()) / 2);
            int y = (int) ((dimension.getHeight() - getHeight()) / 2);
            setLocation(x, y);
            setResizable(false);

            buttonGroup = new ButtonGroup();
            a = new JRadioButton("a", true);
            b = new JRadioButton("b", false);
            c = new JRadioButton("c", false);
            d = new JRadioButton("d", false);
            panelGroup = new JPanel(new GridLayout(4, 1));
            buttonGroup.add(a);
            buttonGroup.add(b);
            buttonGroup.add(c);
            buttonGroup.add(d);

            panelGroup.add(a);
            panelGroup.add(b);
            panelGroup.add(c);
            panelGroup.add(d);
            selectedAnswersList = new ArrayList<>();

            panelGroup2 = new JPanel();
            question = new JLabel("Pytanie");
            btnNext = new JButton("Następne");
            panelGroup2.add(btnNext);

            btnNext.addActionListener(this);

            panel = new JPanel();
            panel.setLayout(new BorderLayout());
            panel.add(question, BorderLayout.NORTH);
            panel.add(panelGroup, BorderLayout.CENTER);
            panel.add(panelGroup2, BorderLayout.SOUTH);
            this.add(panel);
            setVisible(true);
            id = 0;
            WriteQuestion(id);

        }

        void WriteQuestion(int id) {
            question.setText(questions.get(id).getQuestion());
            a.setText(questions.get(id).getAnswer1());
            b.setText(questions.get(id).getAnswer2());
            c.setText(questions.get(id).getAnswer3());
            d.setText(questions.get(id).getAnswer4());
            a.setSelected(true);
            b.setSelected(false);
            c.setSelected(false);
            d.setSelected(false);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == btnNext) {
                if (a.isSelected()) {
                    selectedAnswersList.add(new SelectedAnswers(questions.get(id).getId(), questions.get(id).getIdTest(), questions.get(id).getAnswer1()));
                } else if (b.isSelected()) {
                    selectedAnswersList.add(new SelectedAnswers(questions.get(id).getId(), questions.get(id).getIdTest(), questions.get(id).getAnswer2()));
                } else if (c.isSelected()) {
                    selectedAnswersList.add(new SelectedAnswers(questions.get(id).getId(), questions.get(id).getIdTest(), questions.get(id).getAnswer3()));
                } else if (d.isSelected()) {
                    selectedAnswersList.add(new SelectedAnswers(questions.get(id).getId(), questions.get(id).getIdTest(), questions.get(id).getAnswer4()));
                }
                id++;
                if (id < questions.size()) {
                    WriteQuestion(id);

                } else {
                    dataSender.send(new Message(5, selectedAnswersList));
                    System.out.println("Wysylanie odpowiedzi");
                    btnNext.setEnabled(false);
                    this.dispose();
                }
            }
        }
    }
}
