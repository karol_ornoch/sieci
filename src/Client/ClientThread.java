package Client;

import Model.Message;
import Model.Question;
import Model.Test;
import Model.User;

import javax.swing.*;
import java.io.*;
import java.net.Socket;
import java.util.List;
import java.util.Vector;

/**
 *
 * Klasa obslugujaca polaczenie z serwerem.
 * @author Karol Ornoch
 *
 */

public class ClientThread extends Thread implements Sender {
    private Socket socket;
    private ObjectInputStream in ;
    private ObjectOutputStream out ;
    private Listener dataListener;
    private String host = "localhost";
    private int port =3000;

    /**
     *
     * @param listener obiekt którego interesują dane otrzymane od serwera.
     * @throws UncheckedIOException wyjątek wyrzucany gdy nie
     * @throws IOException wyjątek generowany w przypadku błędów I/O
     *
     */

    public ClientThread(Listener listener) throws UncheckedIOException , IOException{
        dataListener=listener;
        socket=new Socket(host,port);
        this.out = new ObjectOutputStream(socket.getOutputStream());
        out.flush();
        this.in = new ObjectInputStream(socket.getInputStream());
        start();
    }

    @Override
    public void run() {

        System.out.println("Start watku klienta");
        try {
            Object response;
            while((response = in.readObject())!=null){

                Message m = (Message) response;
                switch(m.getType()){
                    case 0:
                        //get user
                        dataListener.reactOnLogin((User)m.getMessage());
                        break;
                    case 1 :
                        //get register message
                        dataListener.reactOnRegister((String)m.getMessage());
                        break;
                    case 2:
                        //get message
                        dataListener.reactOnComment((String)m.getMessage());
                        break;
                    case 3:
                        //get questions
                        dataListener.reactOnQuestionsList((List<Question>)m.getMessage());
                        break;
                    case 4:
                        //get list questions
                        dataListener.reactOnTestList((Vector<Test>)m.getMessage());
                        break;
                    case 6:
                        //update list questions
                        dataListener.reacttOnUpdate((Vector<Test>)m.getMessage());
                        break;
                }
            }
        }catch(IOException e){
            close();
            JOptionPane.showMessageDialog(null,"Brak połaczenia z serwerem");
            e.printStackTrace();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Nadpisana metoda intefejsu Sender.
     */
    @Override
    public void send(Object msg){
        try {
            out.writeObject(msg);
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Nadpisana metoda intefejsu Sender.
     */
    @Override
    public void close() {
        try {
            in.close();
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
