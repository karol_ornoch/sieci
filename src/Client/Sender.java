package Client;

import java.io.IOException;

/**
 * Interfejs posiadajacy metody, ktore powinien widziec obiekt wysylajacy wiadomosc do serwera.
 * @author Karol Ornoch
 */
public interface Sender {
    /**
     * @param msg Obiekt typu mesagge
     */
    void send(Object msg);
    /**
     * Metoda kończąca pracę z obiektem implementującym ten interfejs.
     */
    void close();

}
