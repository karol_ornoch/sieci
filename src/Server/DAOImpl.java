package Server;

import Model.Question;
import Model.SelectedAnswers;
import Model.Test;
import Model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * @author Karol Ornoch
 */
public class DAOImpl implements DAO {
    private Connection connection = null;
    private final String url = "jdbc:postgresql://localhost:5432/BazaTesty";
    private final String username= "postgres";
    private final String password= "admin";

    DAOImpl() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        connection = DriverManager.getConnection(url, username, password);
    }

    @Override
    public boolean saveUser(User user) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute("INSERT INTO users(login,password,isadmin) VALUES ('" + user.getLogin() + "','" + user.getPassword() + "','" + user.isAdmin() + "')");
        return true;
    }

    @Override
    public boolean isExiting(String login) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery("SELECT COUNT(*) FROM users WHERE login = '" + login + "'");
        if (rs.wasNull()) {
            return false;
        }
        return true;
    }

    @Override
    public void addQuestion(Question question) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute("INSERT INTO qustions (testid,question,answer1,answer2,answer3,answer4,correctanswer) " + "VALUES ('" + question.getIdTest() + "','" + question.getQuestion() + "','" + question.getAnswer1() + "','" + question.getAnswer2() + "','" + question.getAnswer3() + "','" + question.getAnswer4() + "','" + question.getIdCorrectAnswer() + "')");
    }

    @Override
    public void addTest(String nametest) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute("INSERT INTO tests (name) VALUES ('" + nametest + "')");
    }

    @Override
    public User getUser(String login, String password) throws SQLException {
        User u = null;
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * FROM users WHERE login = '" + login + "'AND password='" + password + "'");
        if (rs.wasNull()) {
            return null;
        } else {
            while (rs.next()) {
                u = new User(rs.getInt("id"), rs.getString("login"), rs.getString("password"), rs.getBoolean("isadmin"));
            }
        }
        return u;
    }

    @Override
    public List<Question> getQuestionsByIdTest(int idTest) throws SQLException {
        List<Question> questions = new ArrayList<>();

        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * FROM qustions WHERE testid ='" + idTest + "'");
        while (rs.next()) {
            questions.add(new Question(rs.getInt("id"), rs.getString("question"), rs.getString("answer1"), rs.getString("answer2"), rs.getString("answer3"), rs.getString("answer4"), rs.getString("correctanswer"), rs.getInt("testid")));
        }

        return questions;
    }

    @Override
    public Vector<Test> getAllTests() throws SQLException {
        Vector<Test> tests = new Vector<>();
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * FROM tests ");
        while (rs.next()) {
            tests.add(new Test(rs.getInt("id"), rs.getString("name")));
        }
        return tests;
    }

    @Override
    public String checkAnswers(List<SelectedAnswers> selectedAnswerses) throws SQLException {
        double score = 0;
        System.out.println(selectedAnswerses.size());
        for (SelectedAnswers answer : selectedAnswerses) {
            System.out.println(answer.getId() + " " + answer.getAnswer());

            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM qustions WHERE testid ='" + answer.getIdtest() + "'AND id='" + answer.getId() + "'AND correctanswer='" + answer.getAnswer().toString() + "'");

            while (rs.next()) {
                System.out.println(rs.getInt("id") + " ; " + rs.getString("question") + " ; " + rs.getString("answer1") + " ; " + rs.getString("answer2") + " ; " + rs.getString("answer3") + " ; " + rs.getString("answer4") + " ; " + rs.getString("correctanswer") + " ; " + rs.getInt("testid"));
                score++;
            }
        }
        double ocena = 2;
        double procent = score / selectedAnswerses.size();
        if (procent < 0.51) {
            ocena = 2;
        } else if (procent >= 0.51 && procent < 0.61) {
            ocena = 3;
        } else if (procent >= 0.61 && procent < 0.71) {
            ocena = 3.5;
        } else if (procent >= 0.71 && procent < 0.81) {
            ocena = 4;
        } else if (procent >= 0.81 && procent < 0.91) {
            ocena = 4.5;
        } else {
            ocena = 5;
        }

        return "Otrzymałeś ocene: " + ocena;
    }
    //ponizsze metody do zaimplementowania
    @Override
    public void updateQuestion(Question question) throws SQLException {

    }

    @Override
    public void deleteQuestion(int id) throws SQLException {

    }

    @Override
    public void deleteTest(int id) throws SQLException {

    }

}
