package Server;

import Model.Question;
import Model.SelectedAnswers;
import Model.Test;
import Model.User;

import java.sql.SQLException;
import java.util.List;
import java.util.Vector;

/**
 * Interfejs posiadajacy metody pozwalace na dostep do bazy danych.
 *
 * @author Karol Ornoch
 */
public interface DAO {
    boolean saveUser(User user) throws SQLException;

    boolean isExiting(String login) throws SQLException;

    User getUser(String login, String password) throws SQLException;

    void addQuestion(Question question) throws SQLException;

    void addTest(String nametest) throws SQLException;

    List<Question> getQuestionsByIdTest(int idTest) throws SQLException;

    Vector<Test> getAllTests() throws SQLException;

    String checkAnswers(List<SelectedAnswers> selectedAnswerses) throws SQLException;

    void updateQuestion(Question question) throws SQLException;

    void deleteQuestion(int id) throws SQLException;

    void deleteTest(int id) throws SQLException;


}
