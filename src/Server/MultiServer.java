package Server;

import Model.Message;
import Model.Question;
import Model.SelectedAnswers;
import Model.User;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.List;
import java.util.Vector;

/**
 * @author Karol Ornoch
 */
public class MultiServer {

    /**
     * Wektor watkóow obslugujacych klientow
     */
    Vector<ClientThread> v = new Vector<ClientThread>();

    /**
     * Metoda uruchamiajaca serwer
     */
    void runServer() throws IOException {
        //tworzenie gniazda serwera
        ServerSocket server = new ServerSocket(3000);
        System.out.println("Server run ... ");
        while (true) {
            //Akceptacja polaczenia;
            Socket socket = server.accept();
            System.out.println("New client");
            //Tworzenie watku obsugujacego klienta
            ClientThread thread = new ClientThread(socket);
            //dodawanie watku do wektora
            v.addElement(thread);
        }
    }

    /**
     * Glowna metoda programu
     */
    public static void main(String[] args) {
        try {
            new MultiServer().runServer();
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    /**
     * Klasa wewnetrzna dziedziczaca po Thread, obslugujaca polaczenie z klientem
     */
    class ClientThread extends Thread {

        private Socket socket; // gniazdo (połaączenie) klienta
        private ObjectInputStream in;
        private ObjectOutputStream out;
        private DAO dao;
        private User user;

        /**
         * @param socket zaakceptowane polaczenie z klientem
         */
        ClientThread(Socket socket) {

            this.socket = socket;
            try {
                this.in = new ObjectInputStream(socket.getInputStream());
                this.out = new ObjectOutputStream(socket.getOutputStream());
                out.flush();
                this.dao = new DAOImpl();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            start();//uruchamianie wątku
            user = null;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        /**
         * Nadpisana metoda w ciele kt�rej znajduja sie instrukcje wymagajace asynchronicznego wykonania
         */
        public void run() {
            try {
                while (true) {
                    try {
                        Message o = (Message) in.readObject();
                        System.out.println("IN -> " + +o.getType());
                        System.out.println(o.getMessage().getClass().getName());
                        switch (o.getType()) {
                            case 0:
                                System.out.println("Logowanie");
                                User u = (User) o.getMessage();
                                try {
                                    user = dao.getUser(u.getLogin(), u.getPassword());
                                    setUser(user);
                                    send(new Message(0, user));
                                    send(new Message(2, "Zalogowano uzytwnika: " + getUser().getLogin()));
                                } catch (NullPointerException ex) {
                                    send(new Message(2, new String("Błędny login lub hasło")));
                                }
                                break;
                            case 1:
                                System.out.println("Rejestracja");
                                dao.saveUser((User) o.getMessage());
                                send(new Message(1, new String("Utworzono konto")));
                                break;
                            case 3:
                                System.out.println("Pobieranie listy pytan dla testu");
                                List<Question> questions = dao.getQuestionsByIdTest((Integer) o.getMessage());
                                if (questions.isEmpty()) {
                                    send(new Message(2, new String("Ten test nie posiada zadnych pytań")));
                                } else {
                                    send(new Message(3, questions));
                                }
                                break;
                            case 4:
                                System.out.println("Pobiernie testów");
                                send(new Message(4, dao.getAllTests()));
                                break;
                            case 5:
                                System.out.println("Odbieranie odpowiedzi");
                                send(new Message(2, dao.checkAnswers((List<SelectedAnswers>) o.getMessage())));
                                break;
                            case 6:
                                System.out.println("Dodawanie nowego testu");
                                dao.addTest((String) o.getMessage());
                                send(new Message(2, "Dodano nowy test"));
                                send(new Message(6, dao.getAllTests()));
                                break;
                            case 7:
                                System.out.println("Dodawanie pytania do testu");
                                dao.addQuestion((Question) o.getMessage());
                                send(new Message(2, "Dodano nowe pytanie"));
                                break;
                            case 8:
                                System.out.println("Wylogowanie użytkownika");
                                setUser(null);
                                break;
                            case 9:
                                System.out.println("zakonczone przez uzytkowniaka");
                                deleteMeFromVector();
                                break;
                            //nieskonczone
                            case 10:
                                System.out.println("usuwanie pytania");
                                dao.deleteQuestion((Integer) o.getMessage());
                            case 11:
                                System.out.println("usuwanie testu");
                                dao.deleteTest((Integer) o.getMessage());
                            case 12:
                                System.out.println("Edycja pytania");
                                dao.updateQuestion((Question) o.getMessage());

                        }
                    } catch (ClassNotFoundException | SQLException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e) {
                deleteMeFromVector();
            }
        }

        void send(Message msg) {
            try {
                System.out.println("OUT -> " + msg.getType() + " " + msg.getMessage().getClass().getName());
                out.writeObject(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /**
         * Metoda usuwajaca zakończony watek z wektora
         */
        void deleteMeFromVector() {
            if (user != null) {
                System.out.println("User " + getUser().getLogin() + " został usunięty");
            }
            System.out.println("Polaczenie zostalo zakonczone");
            v.remove(this);
        }
    }
}


